# Prerequisites

- [Composer](https://getcomposer.org/)
- PHP 8.1 + extensions

## PHP

For Debian and Ubuntu, if the default PHP version of your distro is older than 8.1,
you should use <https://deb.sury.org/> to install the suitable version.

This site is also known for <https://launchpad.net/~ondrej/+archive/ubuntu/php> (but this covers only Ubuntu)

### PHP extensions needed

- php-intl
- php-mbstring
- php-pdo_pgsql
- php-xml

### PHP 8.1 for Ubuntu 18.04

```shell
sudo apt update
sudo apt install  ca-certificates apt-transport-https software-properties-common
sudo add-apt-repository ppa:ondrej/php
sudo apt update
sudo apt install php8.1 php8.1-xml
```
