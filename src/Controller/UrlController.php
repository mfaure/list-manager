<?php

namespace App\Controller;

use App\Entity\Url;
use App\Repository\UrlRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UrlController extends AbstractController
{
    #[Route('/', name: 'homepage')]
    public function index(UrlRepository $urlRepository): Response
    {
        return $this->render(
            'url/index.html.twig',
            [
                'urls' => $urlRepository->findAll(),
            ]
        );
    }

    #[Route('/url/{id}', name: 'url')]
    public function show(Url $url): Response
    {
        return $this->render(
            'url/show.html.twig',
            [
                'url' => $url,
            ]
        );
    }
}
